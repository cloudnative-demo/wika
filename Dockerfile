#FROM registry.access.redhat.com/ubi8/nginx-120
#FROM default-route-openshift-image-registry.apps.sandbox.x8i5.p1.openshiftapps.com/mayang-yusron-dev/dockerhub-nginx-oci8
#FROM image-registry.openshift-image-registry.svc:5000/openshift/nginx
FROM nginx

RUN cat /etc/*release
RUN cat /etc/passwd
#RUN dnf install wget

# 1001 = default user
# 998:996 = nginx user
USER 1001

#COPY index.html /app
#COPY nginx.conf /etc/nginx/
COPY index.html /opt/app-root/src


EXPOSE 8080
EXPOSE 8443
CMD ["nginx", "-g", "daemon off;"]
